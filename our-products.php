<?php include('header.php'); ?>
<section class="hero-wrap" style="background-image: url('images/industrial-filter-group.jpg'); background-position: center center; height: 300px;">
    <div class="overlay"></div>
    <div class="container">
        <div style="height: 300px;" class="row no-gutters slider-text align-items-end justify-content-start" data-scrollax-parent="true">
            <div class="ftco-animate">
                <p class="breadcrumbs">
                    <span class="mr-2">
                        <a href="index.html">Home <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <span>Our Products <i class="fa fa-chevron-right"></i></span>
                </p>
                <h1 class="mb-3 bread">Our Products</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section" style="padding-bottom: 0">
    <div class="container">
        <div class="justify-content-center mb-1 pb-2">
            <div class="heading-section ftco-animate">
                <h2 class="mb-4">Our Products</h2>
                <p>Our reputation is based on offering better quality product at reasonable price. We have all necessary spare parts in our ready stock & offering 24×7 onsite services.</p>
            </div>
        </div>
        <div class="blog-entry">
            <div class="text bg-light">
                <p style="margin-bottom: 0"><strong><em>We sale varius brand screw air compressor spare parts:</em></strong></p>
                <p>Ally-win, Alup, Atlas Copco, Best Copco, BOgi, Comp Air, DMC, ELGI, Fini, Fushing, Gardner Denver (GD), Hanshing, Hengda, Hitachi, Ingersoll Rand (IR), Jukai, Keaser, Kingston, Kingyon, Kobelco, Kyunyang, Linghin, Lupamat, Mitsui, Puma, Rener, Rich, Roll-air, SCR, Sulair, Xinbao, Yoochang, Ozone Etc.</p>
                <p>For More information please <a href="contact.php">Contact</a></p>
            </div>
        </div>
        <div class="product-groups">
            <h4><strong>Screw Air Compressor Spare Parts</strong></h4>
            <div class="row d-flex">
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/oil-separator.png');"> </a>
                        <h3 class="heading"><a href="">Oil Separator</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/air-filter.jpg');"> </a>
                        <h3 class="heading"><a href="">Air Filter</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/oil-filter.png');"> </a>
                        <h3 class="heading"><a href="">Oil filter</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/oil-seal-and-level-gauge.png');"> </a>
                        <h3 class="heading"><a href="">Oil Seal & Level Gauge</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/compressor-oil.jpg');"> </a>
                        <h3 class="heading"><a href="">Compressor oil</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/line-filter.jpg');"> </a>
                        <h3 class="heading"><a href="">Line Filter</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/pressure-and-pemperature-sensor.jpg');"> </a>
                        <h3 class="heading"><a href="">Pressure & Temperature sensor</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/invertar.png');"> </a>
                        <h3 class="heading"><a href="">Invertar</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/cupling.jpg');"> </a>
                        <h3 class="heading"><a href="">Coupling</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/soienoid-valve.jpg');"> </a>
                        <h3 class="heading"><a href="">Soienoid Valve</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/cooling-fan-and-bearing-bush.jpg');"> </a>
                        <h3 class="heading"><a href="">Cooling Fan & Bearing Bush</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/air-compressor-controller-ct.png');"> </a>
                        <h3 class="heading"><a href="">Air Compressor Controller & CT</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/plc.jpg');"> </a>
                        <h3 class="heading"><a href="">PLC</a></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-groups">
            <h4><strong>Boiler Spare Parts</strong></h4>
            <div class="row d-flex">
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/burner-controller-and-display.jpg');"> </a>
                        <h3 class="heading"><a href="">Burner Controller and Display</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/gas-regulator-and-photocell.jpg');"> </a>
                        <h3 class="heading"><a href="">Gas Regulator and Photocell</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/dosing-pump.jpg');"> </a>
                        <h3 class="heading"><a href="">Dosing Pump</a></h3>
                    </div>
                </div>
                <div class="col-lg-3 ftco-animate fadeInUp ftco-animated">
                    <div class="blog-entry">
                        <a href="" class="block-20" style="background-image: url('images/products/modulator-and-safty-valve.jpg');"> </a>
                        <h3 class="heading"><a href="">Modulator and Safty Valve</a></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php include('footer.php'); ?>