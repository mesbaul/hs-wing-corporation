<?php include('header.php'); ?>
<section class="hero-wrap" style="background-image: url('images/industrial-filter-group.jpg'); background-position: center center; height: 300px;">
    <div class="overlay"></div>
    <div class="container">
        <div style="height: 300px;" class="row no-gutters slider-text align-items-end justify-content-start" data-scrollax-parent="true">
            <div class="ftco-animate">
                <p class="breadcrumbs">
                    <span class="mr-2">
                        <a href="index.html">Home <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <span>About Us <i class="fa fa-chevron-right"></i></span>
                </p>
                <h1 class="mb-3 bread">About Us</h1>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-counter ftco-no-pt ftco-no-pb" id="about-section">
    <div class="container">
        <div class="justify-content-center mb-4 pt-md-4">
            <div class="heading-section ftco-animate">
                <h2 class="mb-4">We are <?php echo $config['brand_name']; ?></h2>
                <p>Due to respect we would like to inform you that we are providing Compressor spare parts (oil filter, air filter, oil separator)  and Compressor oil-46/68, also Boiler spare parts and Service. We proudly say that Our quality is the best and most reasonable price.</p>
                <p style="margin: 0;"><em>Sir, please contract with us for any query.</em></p>
                <p style="margin: 0;"><?php echo $config['address']; ?></p>
                <p style="margin: 0;"><a href="tel:<?php echo $config['phone'][0]; ?>"><?php echo $config['phone'][0]; ?></a>, <a href="tel:<?php echo $config['phone'][1]; ?>"><?php echo $config['phone'][1]; ?></a>, <a href="tel:<?php echo $config['phone'][2]; ?>"><?php echo $config['phone'][2]; ?></a></p>
                <p style="margin: 0;"><a href="mailto:<?php echo $config['mail'][0]; ?>"><?php echo $config['mail'][0]; ?></a></p>
                <p style="margin: 0;"><a href="mailto:<?php echo $config['mail'][1]; ?>"><?php echo $config['mail'][1]; ?></a></p>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 bg-primary">
    <div class="container">
        <div class="py-lg-5">
            <div class="justify-content-center pb-5">
                <div class="heading-section ftco-animate">
                    <h2 class="mb-3">Why Choose To <?php echo $config['brand_name']; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-customer-service"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">24/7 Customer Service</h3>
                            <p>24/7 Compressor Spare parts delivery &amp; service support.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">Prompt Delivery</h3>
                            <p>We do deliver Customer’s order in times according to their expected schedule. Wheather respected Customer wants their required order by urgent basis, we do serve it as well without extra fees.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">Reliable Equipment</h3>
                            <p>We assure you to deliver Customer’s product without any damage with proper quality factor & service support after delivery. Reasonable care for pricing and adequate product warranty service than others.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">New Heavy Equipment</h3>
                            <p>We do deliver our listing brand companies product as well as respected Client’s required & reliable brand companies product.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>