<?php include('header.php'); ?>
<section class="hero-wrap js-fullheight" style="background-image: url('images/industrial-filter-group.jpg'); background-position: 50% 0;" data-stellar-background-ratio="1">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-lg-9 ftco-animate text-center">
                <div class="mt-5">
                    <h1 class="mb-4"><?php echo $config['slogan']; ?></h1>
                    <p class="mb-4">A lot of people have fancy things to say about customer service, but it’s just a day-in, day-out, ongoing, never-ending, persevering, compassionate kind of activity.</p>
                    <p style="display: none;"><a href="contact.php" class="btn btn-primary">Contact us</a> <a href="contact.php" class="btn btn-white">Request A Quote</a></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-no-pt ftco-no-pb ftco-services-2 bg-primary">
    <div class="container">
        <div class="py-lg-5">
            <div class="justify-content-center pb-5">
                <div class="heading-section ftco-animate">
                    <h2 class="mb-3">Why Choose To <?php echo $config['brand_name']; ?></h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-customer-service"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">24/7 Customer Service</h3>
                            <p>24/7 Compressor Spare parts delivery &amp; service support.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">Prompt Delivery</h3>
                            <p>We do deliver Customer’s order in times according to their expected schedule. Wheather respected Customer wants their required order by urgent basis, we do serve it as well without extra fees.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">Reliable Equipment</h3>
                            <p>We assure you to deliver Customer’s product without any damage with proper quality factor & service support after delivery. Reasonable care for pricing and adequate product warranty service than others.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services d-flex">
                        <div class="icon justify-content-center align-items-center d-flex"><span class="flaticon-road-roller"></span></div>
                        <div class="media-body pl-4">
                            <h3 class="heading mb-3">New Heavy Equipment</h3>
                            <p>We do deliver our listing brand companies product as well as respected Client’s required & reliable brand companies product.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-counter ftco-no-pt ftco-no-pb" id="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 d-flex align-items-stretch">
                <div class="about-wrap display-table">
                    <div class="display-table-cell text-center">
                        <img src="images/compressor-air-filter.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-6 py-5 pl-md-5">
                <div class="row justify-content-center mb-4 pt-md-4">
                    <div class="col-md-12 heading-section ftco-animate">
                        <h2 class="mb-4">Welcome to <p><?php echo $config['brand_name']; ?></p></h2>
                        <p>Best quality, Reasonable price, First service" - it's our commitment. As members of the global community, we will work for the betterment of society. Customer satisfaction is our first priority.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number">1</strong>
                                <span>Years of Experienced</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number">120+</strong>
                                <span>Customers Served</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number">300</strong>
                                <span>Number of Equipment</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-flex justify-content-center counter-wrap ftco-animate">
                        <div class="block-18">
                            <div class="text">
                                <strong class="number">10+</strong>
                                <span>Number of Staffs</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-intro img">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h2>We Are <?php echo $config['brand_name']; ?> Company</h2>
                <p><?php echo $config['slogan']; ?></p>
                <p class="mb-0"><a href="contact.php" class="btn btn-primary px-4 py-3">Make An Appointment</a></p>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section ftco-counter" id="about-section">
    <div class="container">
        <div class="row">
            <div class="col-md-6 ftco-animate">
                <div style="display: table; height: 100%; width: 100%;">
                    <div style="display: table-cell; vertical-align: middle; text-align: center;">
                        <img class="img w-50" src="images/qr-code.png">
                    </div>
                </div>
            </div>
            <div class="col-md-6 py-5 pl-md-5">
                <div class="row justify-content-center mb-4 pt-md-4">
                    <div class="col-md-12 heading-section ftco-animate">
                        <h2 class="mb-4">SCAN QR Code</h2>
                        <p>We offer fast service and good quality at low price. Scan QR code to know more about us.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>