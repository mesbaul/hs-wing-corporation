<?php include('configs.php'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo $config['brand_name']; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&amp;display=swap" rel="stylesheet" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/animate.css" />
        <link rel="stylesheet" href="css/owl.carousel.min.css" />
        <link rel="stylesheet" href="css/owl.theme.default.min.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/bootstrap-datepicker.css" />
        <link rel="stylesheet" href="css/jquery.timepicker.css" />
        <link rel="stylesheet" href="css/flaticon.css" />
        <link rel="stylesheet" href="css/style.css" />
    </head>
    <body>
        <div class="py-3">
            <div class="container">
                <div class="row d-flex align-items-start align-items-center px-3 px-md-0">
                    <div class="col-md-4 d-flex mb-2 mb-md-0">
                        <a class="navbar-brand d-flex align-items-center" href="index.php"><?php echo $config['brand_name']; ?></a>
                    </div>
                    <div class="col-md-4 d-flex topper mb-md-0 mb-2 align-items-center">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="fa fa-map"></span>
                        </div>
                        <div class="pr-md-4 pl-md-3 pl-3 text">
                            <p class="con"><span>Free Call</span>&nbsp;<span><?php echo $config['phone'][0]; ?></span>,&nbsp;<span><?php echo $config['phone'][1]; ?></span></p>
                            <p class="con"><span></span><span><?php echo $config['phone'][2]; ?></span></p>
                            <p class="con">Call Us Now 24/7 Customer Support</p>
                        </div>
                    </div>
                    <div class="col-md-4 d-flex topper mb-md-0 align-items-center">
                        <div class="icon d-flex justify-content-center align-items-center"><span class="fa fa-paper-plane"></span></div>
                        <div class="text pl-3 pl-md-3">
                            <p class="hr"><span>Our Location</span></p>
                            <p class="con"><?php echo $config['address']; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $link = $_SERVER['PHP_SELF'];
        $link_array = explode('/', $link);
        $page = end($link_array);
        ?>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
            <div class="container d-flex align-items-center">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation"><span class="fa fa-bars"></span> Menu</button>
                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item <?php echo ($page == 'index.php') ? 'active' : '' ?>"><a href="index.php" class="nav-link">Home</a></li>
                        <li class="nav-item <?php echo ($page == 'about.php') ? 'active' : '' ?>"><a href="about.php" class="nav-link">About</a></li>
                        <li class="nav-item <?php echo ($page == 'our-products.php') ? 'active' : '' ?>"><a href="our-products.php" class="nav-link">Our Products</a></li>
                        <li class="nav-item <?php echo ($page == 'contact.php') ? 'active' : '' ?>"><a href="contact.php" class="nav-link">Contact</a></li>
                    </ul>
                    <a style="display: none;" href="contact.php" class="request-quote">Request A Quote</a>
                </div>
            </div>
        </nav>