        <footer class="ftco-footer">
            <div class="container mb-5 pb-4">
                <div class="row">
                    <div class="col-lg col-md-6">
                        <div class="ftco-footer-widget">
                            <h2 class="ftco-heading-2 logo d-flex align-items-center">
                                <a href="index.php"><?php echo $config['brand_name']; ?></a>
                            </h2>
                            <p><?php echo $config['slogan']; ?></p>
                            <ul class="ftco-footer-social list-unstyled mt-4">
                                <li>
                                    <a href="<?php echo $config['fb_link'] ?>"><span class="fa fa-facebook"></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg col-md-6">
                        <div class="ftco-footer-widget">
                            <h2 class="ftco-heading-2">Contact Us</h2>
                            <div class="block-23 mb-3">
                                <ul>
                                    <li>
                                        <span class="fa fa-map-marker mr-3"></span>
                                        <span class="text"><?php echo $config['address']; ?></span>
                                    </li>
                                    <li>
                                        <a style="margin-bottom:0" href="tel:<?php echo $config['phone'][0]; ?>">
                                            <span class="fa fa-phone mr-3"></span>
                                            <span class="text"><?php echo $config['phone'][0]; ?></span>
                                            <span class="">,&nbsp;</span>
                                            <span class="text"><?php echo $config['phone'][1]; ?></span>
                                        </a>
                                        <a href="tel:<?php echo $config['phone'][1]; ?>">
                                        </a>
                                    </li>
                                    <li>
                                        <a style="margin-bottom:0" href="mailto:<?php echo $config['mail'][0]; ?>">
                                            <span class="fa fa-paper-plane mr-3"></span>
                                            <span class="text"><?php echo $config['mail'][0]; ?></span>
                                        </a>
                                        <a href="mailto:<?php echo $config['mail'][1]; ?>">
                                            <span class="mr-3">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            <span class="text"><?php echo $config['mail'][1]; ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid bg-primary py-5">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="mb-0">Copyright &copy; <?php echo date('Y') ?> All rights reserved | <a href="<?php echo $config['website']; ?>"><?php echo $config['brand_name']; ?></a></p>
                    </div>
                </div>
            </div>
        </footer>
        <script src="js/jquery.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>
        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.animateNumber.min.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/jquery.timepicker.min.js"></script>
        <script src="js/scrollax.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
