<?php
$config = [
    'brand_name' => 'HS Wing Corporation',
    'slogan' => 'Customer satisfaction is our First priority',
    'mail' => [
    	'humaunsumi@gmail.com',
    	'hswingcorporation@gmail.com'
    ],
    'phone' => [
    	'01741-532317',
    	'01611-532317',
    	'01826-188537'
    ],
    'website' => 'http://hswingcorporation.com/',
    'address' => 'H-301, (2nd Floor), Ashkona Collage Road, Rosulbag Area, Near Haji School, Dakkhinkhan, Uttara, Dhaka-1230',
    'fb_link' => 'https://www.facebook.com/HumaunHimu'
];
?>