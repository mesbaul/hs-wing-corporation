<?php include('header.php'); ?>
<section class="hero-wrap" style="background-image: url('images/industrial-filter-group.jpg'); background-position: center center; height: 300px;">
    <div class="overlay"></div>
    <div class="container">
        <div style="height: 300px;" class="row no-gutters slider-text align-items-end justify-content-start" data-scrollax-parent="true">
            <div class="ftco-animate">
                <p class="breadcrumbs">
                    <span class="mr-2">
                        <a href="index.html">Home <i class="fa fa-chevron-right"></i></a>
                    </span>
                    <span>Contact Us <i class="fa fa-chevron-right"></i></span>
                </p>
                <h1 class="mb-3 bread">Contact Us</h1>
            </div>
        </div>
    </div>
</section>
</section>
<section class="ftco-section contact-section ftco-no-pb" id="contact-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate fadeInUp ftco-animated">
                <span class="subheading">Contact us</span>
                <h2 class="mb-4"><?php echo $config['brand_name']; ?>?</h2>
                <p><?php echo $config['slogan']; ?></p>
            </div>
        </div>
        <div class="row block-9">
            <div class="col-md-8">
                <form action="send_mail.php" method="POST" class="p-4 p-md-5 contact-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Your Name" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Your Email" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="text" name="subject" class="form-control" placeholder="Subject" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <textarea name="message" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="submit" name="submit_as" value="Send Message" class="btn btn-primary py-3 px-5" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 d-flex pl-md-5">
                <div class="row">
                    <div class="dbox w-100 d-flex ftco-animate fadeInUp ftco-animated">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-map-marker"></span>
                        </div>
                        <div class="text">
                            <p><span>Address:</span> <?php echo $config['address']; ?></p>
                        </div>
                    </div>
                    <div class="dbox w-100 d-flex ftco-animate fadeInUp ftco-animated">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-phone"></span>
                        </div>
                        <div class="text">
                            <p><span>Phone:</span> <a href="tel:<?php echo $config['phone'][0]; ?>"><?php echo $config['phone'][0]; ?></a></p>
                            <p><a href="tel:<?php echo $config['phone'][1]; ?>"><?php echo $config['phone'][1]; ?></a></p>
                            <p><a href="tel:<?php echo $config['phone'][2]; ?>"><?php echo $config['phone'][2]; ?></a></p>
                        </div>
                    </div>
                    <div class="dbox w-100 d-flex ftco-animate fadeInUp ftco-animated">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-paper-plane"></span>
                        </div>
                        <div class="text">
                            <p><span>Email:</span> <a href="mailto:<?php echo $config['mail'][0]; ?>"><?php echo $config['mail'][0]; ?></a></p>
                            <p><a href="mailto:<?php echo $config['mail'][1]; ?>"><?php echo $config['mail'][1]; ?></a></p>
                        </div>
                    </div>
                    <div class="dbox w-100 d-flex ftco-animate fadeInUp ftco-animated">
                        <div class="icon d-flex align-items-center justify-content-center">
                            <span class="fa fa-globe"></span>
                        </div>
                        <div class="text">
                            <p><span>Website</span> <a href="#"><?php echo $config['website']; ?></a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div id="x" class="x" style="position: relative; overflow: hidden;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3649.0741801863032!2d90.41536891435041!3d23.851499490824956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c67bfd93a27f%3A0x8fb6f83c6c24504f!2zQWlycG9ydCAtIERha3NoaW5raGFuIFJkLCDgpqLgpr7gppXgpr4gMTIzMA!5e0!3m2!1sbn!2sbd!4v1611038410700!5m2!1sbn!2sbd" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('footer.php'); ?>